from lib2to3.pytree import Base
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Table, Column, Integer, ForeignKey

db = SQLAlchemy()


class User(db.Model):
    #__tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    email = db.Column(db.Text, nullable=True)
    role = db.Column(db.Text, nullable=False)   # не понятно какой тип данных
    location = db.Column(db.Text, nullable=True)
    requisites = db.Column(db.Integer, unique=True, nullable=False)        #orders = db.relationship('order', backref='user', lazy=True)
    #order_id = db.Column(
        #db.Integer,
        #db.ForeignKey('order.id', ondelete='CASCADE'),
        #index=True
    #)


def __repr__(self):
    return '< {} {} {}>'.format(self.role, self.location, self.requisites)


class Order(db.Model):
    #__tablename__ = 'order'
    #order_Id: int
    #order_Namber: str
    # ...
    #date_start: str



    id = db.Column(db.Integer, primary_key=True)
    manufacturer_id = db.Column(db.Integer, ForeignKey('user.id'), index=True)                  # ForeignKey('user.id')
    customer_id = db.Column(db.Integer, ForeignKey('user.id'), index=True)     #ForeignKey('user.id')
        #order_Namber = db.Column(db.String, nullable=False)
    price = db.Column(db.String, nullable=False)
    date_start = db.Column(db.DateTime, nullable=False)
    production_period = db.Column(db.DateTime, nullable=False)
    order_placement_date = db.Column(db.DateTime, nullable=False)
    date_end = db.Column(db.DateTime, nullable=False)
    state = db.Column(db.Text, nullable=False)  # не понятно какой тип данных

    #def __repr__(self):
        #return '< Стоймость заказа {}' \
               #'Дата создания заказа {}' \
               #'Производственный период {}' \
               #'Дата размещения заказа {}' \
               #'Крайняя дата изготовления заказа {}' \
               #'Статус заказа {}>'.format(self.price, self.date_start, self.production_period, self.order_placement_date, self.date_end, self.state)

    #user_id = db.Column(
        #db.Integer,
        #db.ForeignKey('user.id', ondelete='CASCADE'),
        #index=True
    #)

#class RolesInOrders(db.Model):
    #user_id = db.Column(db.Integer, primary_key=True)
    #order_Id = db.Column(db.Integer, primary_key=True)


class password (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.Text, nullable=True)
    user_id = db.Column(
        db.Integer,
        db.ForeignKey('user.id', ondelete='CASCADE'),
        index=True
    )

    def __repr__(self):
        return '< {} >'.format(self.key)
