#from flask import Flask, render_template, jsonify
#from flask_login import LoginManager
#from back_end import db, User, Order


#from flask import Flask, request, jsonify, flask_marshmallow
#from flask_sqlalchemy import SQLAlchemy
#from flask_marshmallow import Marshmallow
#import os


#basedir = os.path.abspath(os.path.dirname(__file__))





from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

# Инициализация приложения
app = Flask(__name__)
app.config.from_pyfile('config.py')
app.debug = True

# Инициализация работы с базой
db = SQLAlchemy(app)
ma = Marshmallow(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    email = db.Column(db.String(80), nullable=True)
    role = db.Column(db.Text, nullable=False)
    location = db.Column(db.Text, nullable=True)
    requisites = db.Column(db.Integer, unique=True, nullable=False)

class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    manufacturer_id = db.Column(db.Integer, ForeignKey('user.id'), index=True)  # ForeignKey('user.id')
    customer_id = db.Column(db.Integer, ForeignKey('user.id'), index=True)  # ForeignKey('user.id')
    price = db.Column(db.String, nullable=False)
    date_start = db.Column(db.DateTime, nullable=False)
    production_period = db.Column(db.DateTime, nullable=False)
    order_placement_date = db.Column(db.DateTime, nullable=False)
    date_end = db.Column(db.DateTime, nullable=False)
    state = db.Column(db.Text, nullable=False)

# Cхема
class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("email", "date_created")


user_schema = UserSchema()
users_schema = UserSchema(many=True)

class OrderSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("email", "date_created")


db.create_all()


# 1Маршрут получения данных пользователя по id
@app.route("/user/<id>", methods=["GET"])
def user_detail(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


# 2Маршрут получения данных заказа по id
@app.route("/order/<id>", methods=["GET"])
def order_detail(id):
    order = Order.query.get(id)
    return order_schema.jsonify(order)

# 3Маршрут для создания пользователя
@app.route("/add_user", methods=["POST"])
def user_add():
    email = request.json['email']
    key = request.json['key']
    new_user = User(email, key)

    db.session.add(new_user)
    db.session.commit()

    return jsonify(new_user)



# 4Маршрут для создания данных по заказу
@app.route("/add_order", methods=["POST"])
def add_order():
    ordername = request.json['ordername']
    email = request.json['email']

    new_order = Order(ordername, email)

    db.session.add(new_order)
    db.session.commit()

    return jsonify(new_order)


#5 Маршрут для авторизации пользователей
@app.route("/user/authorization", methods=["GET"])
def user_authorization(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


#6 Маршрут для получения всех заказов по пользователю
@app.route("/user_orders", methods=["GET"])
def user_detail(id):
    user_orders = User.query.get(id)
    return user_schema.jsonify(user_orders)



# Маршрут для просмотра пользователей
#@app.route("/api/users/")
#def get_users():
    #all_users = User.query.all()
    #result = users_schema.dump(all_users)
    #return jsonify(result)



# Маршрут для создания пользователя
#@app.route("/api/user/create")
#def create_user():
    #user = User()
    #user.password = 'test'
    #user.email = 'allalalala@gmail.com'

    #db.session.add(user)
    #db.session.commit()
    #return user_schema.jsonify(user)










#def create_app():
    #app = Flask(__name__)
    #app.config.from_pyfile('config.py')
    #db.init_app(app)


    #login_manager = LoginManager()
    #login_manager.init_app(app)
    #login_manager.login_view = 'login'


# endpoint to get user detail by id
    #@app.route("/user/<id>", methods=["GET"])
    #def user_detail(id):
        #user = User.query.get(id)
        #return user_schema.jsonify(user)





    #@app.route('/customer_cab.html', page_title=title)
    #def index():
        #title = "Кабинет заказчика"
        #return render_template('d2_customer_cab.html', page_title=title)    #шаблон кабинета заказчика



    #return app