from flask import Blueprint, flash, render_template, redirect, url_for
from flask_login import current_user, login_user, logout_user

from back_end import db
from back_end import LoginForm, RegistrationForm, ContractorForm, ContChoiseForm, CustomerForm, OrderStatus
from back_end import User

blueprint = Blueprint('user', __name__, url_prefix='/users')

@blueprint.route('/login', methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('start'))
    title = "Авторизация"
    login_form = LoginForm()
    return render_template('3D/d10_d11_login.html', page_title=title, form=login_form)

@blueprint.route('/process-login', methods=['POST'])
def process_login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and user.check_password(form.password.data):
            if form.order_role.data == 'customer':
                login_user(user)
                flash('Вы вошли на сайт как заказчик')
                return redirect(url_for('user.customer_cab'))
            elif form.order_role.data == 'contractor':
                login_user(user)
                flash('Вы вошли на сайт как исполнитель')
                return redirect(url_for('user.contractor_cab'))

    flash('Неправильное имя пользователя или пароль')
    return redirect(url_for('user.login'))

@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('start'))

@blueprint.route('/register', methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('start'))
    form = RegistrationForm()
    title = "Регистрация"
    return render_template('user/registration.html', page_title=title, form=form)

@blueprint.route('/process-reg', methods=['POST'])
def process_reg():
    form = RegistrationForm()
    if form.validate_on_submit():
        new_user = User(username=form.username.data, role='user')
        new_user.set_password(form.password.data)
        db.session.add(new_user)
        db.session.commit()
        flash('Вы успешно зарегистрировались!')
        return redirect(url_for('user.login'))
    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash('Ошибка в поле "{}": - {}'.format(
                    getattr(form, field).label.text,
                    error
                ))
        return redirect(url_for('user.register'))
    flash('Пожалуйста, исправьте ошибки в форме')
    return redirect(url_for('user.register'))

@blueprint.route('/contractor_cab', methods=['GET','POST'])
def contractor_cab():
    title = "Кабинет исполнителя"
    ready = False # last value from DB
    contractor_form = ContractorForm()
    return render_template('3D/d9_contractor_cab.html', page_title=title, form=contractor_form)

@blueprint.route('/contractor_save', methods=['GET','POST'])
def contractor_save():
    form = ContractorForm()
    status_form = OrderStatus()
    if form.save.data:
        if form.validate_on_submit():
            if form.ready_for_order.data == 'yes':
                ready = True
                title = "Кабинет исполнителя"
                flash('Данные сохранены. Вы готовы принять заказ')
                return render_template('3D/d9_contractor_cab.html', page_title=title, form=form, ready=ready)
            elif form.ready_for_order.data == 'no':
                flash('Данные сохранены. Вы НЕ готовы принять заказ')
                ready = False
                title = "Кабинет исполнителя"
                return render_template('3D/d9_contractor_cab.html', page_title=title, form=form, ready=ready)
    elif form.new_order.data:
        title = "Новый заказ" 
        return render_template('3D/d12_new_order_accept.html', page_title=title)
    elif form.order.data:
        title = "Заказ №" 
        return render_template('3D/d13_order_in_process.html', page_title=title, form=status_form)   

@blueprint.route('/get_file', methods=['GET','POST'])
def get_file():
    with open('file.txt', 'w', encoding='utf-8') as new_file:
        new_file.write("3D Print MODEL")
    flash('Файл загружен')
    title = "Новый заказ" 
    # if current_user = contractor
    return render_template('3D/d12_new_order_accept.html', page_title=title)
    # elif current_user = customer
    # return render_template('3D/d5_order.html', page_title=title)

@blueprint.route('/get_file_order', methods=['GET','POST'])
def get_file_order():
    form = OrderStatus()
    with open('file.txt', 'w', encoding='utf-8') as new_file:
        new_file.write("3D Print MODEL")
    flash('Файл загружен')
    title = "Заказ №" 
    return render_template('3D/d13_order_in_process.html', page_title=title, form=form)

@blueprint.route('/order_accepted', methods=['GET','POST'])
def order_accepted():
    title = "Заказ принят" 
    return render_template('3D/d14_order_accepted.html', page_title=title)

@blueprint.route('/order_status', methods=['GET','POST'])
def order_status():
    form = OrderStatus()
    if form.no_done.data:
        title = "Заказ №" 
        flash('Статус заказа изменен на: НЕ ПОЛУЧИЛОСЬ СДЕЛАТЬ')
        return render_template('3D/d13_order_in_process.html', page_title=title, form=form)
    elif form.inwork.data:
        title = "Заказ №" 
        flash('Статус заказа изменен на: В РАБОТЕ')
        return render_template('3D/d13_order_in_process.html', page_title=title , form=form)
    elif form.done.data:
        title = "Заказ №" 
        flash('Статус заказа изменен на: ВЫПОЛНЕН')
        return render_template('3D/d13_order_in_process.html', page_title=title , form=form)

@blueprint.route('/customer_cab', methods=['GET','POST'])
def customer_cab():
    title = "Кабинет заказчика"
    form = CustomerForm()
    return render_template('3D/d2_customer_cab.html', page_title=title, form=form)

@blueprint.route('/my_order', methods=['GET','POST'])
def my_order():
    title = "Мой заказ №"
    form = CustomerForm()
    return render_template('3D/d5_order.html', page_title=title, form=form)

@blueprint.route('/new_order', methods=['GET','POST'])
def new_order():
    title = "Новый заказ"
    form = CustomerForm()
    return render_template('3D/d3_make_order.html', page_title=title, form=form)

@blueprint.route('/load_file', methods=['GET','POST'])
def load_file():
    #with open('file.txt', 'w', encoding='utf-8') as new_file:
     #   new_file.write("3D Print MODEL")
    form = CustomerForm()
    flash('Файл загружен')
    title = "Новый заказ" 
    return render_template('3D/d3_make_order.html', page_title=title, form=form)

@blueprint.route('/save_new_order', methods=['GET','POST'])
def save_new_order():
    title = "Заказ №"
    form = CustomerForm()
    choise_form = ContChoiseForm()
    if form.validate_on_submit():
        return render_template('3D/d4_new_order.html', page_title=title, form=choise_form)
    flash('Неверно заполнены данные')
    return redirect(url_for('user.new_order'))

@blueprint.route('/new_order_screen', methods=['GET','POST'])
def new_order_screen():
    title = "Заказ №"
    choise_form = ContChoiseForm()
    return render_template('3D/d4_new_order.html', page_title=title, form=choise_form)

@blueprint.route('/contractor_choise', methods=['GET','POST'])
def contractor_choise():
    title = "Кабинет заказчика"
    form = CustomerForm()
    return render_template('3D/d2_customer_cab.html', page_title=title, form=form)