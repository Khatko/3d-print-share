from flask_wtf import FlaskForm
from wtforms import BooleanField, IntegerField, PasswordField, RadioField, StringField, SelectField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError

from back_end import User

class LoginForm(FlaskForm):
    username = StringField('Введите адрес эл.почты', validators=[DataRequired(), Email()], render_kw={"class": "form-control", "placeholder": "Email"})
    password = PasswordField('Введите пароль', validators=[DataRequired()], render_kw={"class": "form-control", "placeholder": "Password"})
    order_role = SelectField('Выберите роль', choices=[('customer', 'Заказчик'), ('contractor', 'Исполнитель')], render_kw={"class": "form-control"})
    remember_me = BooleanField('Запомнить меня', default=True, render_kw={"class": "form-check-input"})
    submit = SubmitField('Войти', render_kw={"class": "btn btn-primary"})

class RegistrationForm(FlaskForm):
    username = StringField('Укажите вашу почту', validators=[DataRequired(), Email()], render_kw={"class": "form-control", "placeholder": "Email"})
    password = PasswordField('Пароль', validators=[DataRequired()], render_kw={"class": "form-control"})
    password2 = PasswordField('Повторите пароль', validators=[DataRequired(), EqualTo('password')], render_kw={"class": "form-control"})
    submit = SubmitField('Отправить', render_kw={"class": "btn btn-primary"})

    def validate_username(self, username):
        users_count = User.query.filter_by(username=username.data).count()
        if users_count > 0:
            raise ValidationError('Пользователь с таким именем уже зарегистрирован')

class ContractorForm(FlaskForm):
    ready_for_order = RadioField('Готовность принять заказ', validators=[DataRequired()], choices=[('yes', 'Да'), ('no', 'Нет')], render_kw={"class": "form-check"})
    min_price = IntegerField('Укажите минимальную цену', validators=[DataRequired()], render_kw={"class": "form-control",})
    min_days = IntegerField('Укажите срок изготовления в днях', validators=[DataRequired()], render_kw={"class": "form-control"})
    adress = StringField('Введите адрес производства', validators=[DataRequired()], render_kw={"class": "form-control"})
    save = SubmitField('Сохранить', render_kw={"class": "btn btn-light"})
    order = SubmitField('Заказ №', render_kw={"class": "btn btn-primary"})

class CustomerForm(FlaskForm):
    min_price = IntegerField('Укажите максимальную цену', validators=[DataRequired()], render_kw={"class": "form-control",})
    max_days = IntegerField('Укажите срок изготовления в днях', validators=[DataRequired()], render_kw={"class": "form-control"})
    adress = StringField('Введите адрес производства', validators=[DataRequired()], render_kw={"class": "form-control"})
    save = SubmitField('Сохранить заказ', render_kw={"class": "btn btn-light"})
    new_order = SubmitField('Заказ №', render_kw={"class": "btn btn-info"})
    order = SubmitField('Заказ №', render_kw={"class": "btn btn-primary"})

class ContChoiseForm(FlaskForm):
    contractor = SubmitField('Исполнитель №', render_kw={"class": "btn btn-primary"})
    
class OrderStatus(FlaskForm):
    no_done = SubmitField('Не получилось', render_kw={"class":"btn btn-info btn-lg", "style":"background-color:rgb(250, 0, 71)"})
    inwork = SubmitField('В работе', render_kw={"class": "btn btn-info btn-lg", "style":"background-color:Gray"})
    done = SubmitField('Готов', render_kw={"class": "btn btn-info btn-lg", "style":"background-color:rgb(60, 169, 71)"})

   

    #def validate_email(self, email):
    #    users_count = User.query.filter_by(email=email.data).count()
    #    if users_count > 0:
    #        raise ValidationError('Пользователь с такой электронной почтой уже зарегистрирован')
