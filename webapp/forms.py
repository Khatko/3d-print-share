from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    useremail = StringField('Email', validators=[DataRequired()])            #нужен email validator?
    password = PasswordField('Пароль', validators=[DataRequired()])
    submit = SubmitField('Отправить')